package com.students.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.students.Repositories.FormationRepository;
import com.students.Repositories.StudentRepository;
import com.students.entities.Formation;
import com.students.entities.Student;

@CrossOrigin
@RestController
public class FormationController {
	
	private final FormationRepository formationRepository;
	private final StudentRepository studentRepository;
	
	@Autowired
	public FormationController (FormationRepository formationRepository, StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
        this.formationRepository = formationRepository;
	}
	
	@GetMapping("/allFormations")
	public List<Formation> getFormations(){
		return formationRepository.findAll();
	}

	@GetMapping("/allStudents")
	public List<Student> getStudents(){
		return studentRepository.findAll();
	}
	
	@GetMapping("/formation/{id}/students")
	public List<Student> getStudentsByFormation(@PathVariable("id") Long id){
		return studentRepository.findByFormationId(id);
	}
}
