package com.students.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "formation")
public class Formation {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nom_formation")
	private String nomFormation;
	private int duree;
	
	@OneToMany(mappedBy = "formation")
	private List<Student> students;
	
	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Formation(String nomFormation, int duree) {
		super();
		this.nomFormation = nomFormation;
		this.duree = duree;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomFormation() {
		return nomFormation;
	}

	public void setNomFormation(String nomFormation) {
		this.nomFormation = nomFormation;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", nomFormation=" + nomFormation + ", duree=" + duree + "]";
	}
}
