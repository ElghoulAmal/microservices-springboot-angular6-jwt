package com.students.projections;

import org.springframework.data.rest.core.config.Projection;
import com.students.entities.Student;

@Projection(name = "p1", types = {Student.class})
public interface StudentProjection {
	public String getNom();
}
