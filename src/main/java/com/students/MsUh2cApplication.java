package com.students;

//import java.util.List;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import com.students.Repositories.FormationRepository;
//import com.students.Repositories.StudentRepository;
//import com.students.entities.Formation;
//import com.students.entities.Student;
//import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MsUh2cApplication {
	//public class MsUh2cApplication implements CommandLineRunner{

//	@Autowired
//	private StudentRepository repository;
//	
//	@Autowired
//	private FormationRepository formationRepository;
//	
//	@Autowired
//	private RepositoryRestConfiguration repositoryRestConfiguration;
	
	public static void main(String[] args) {
		SpringApplication.run(MsUh2cApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		
//		// Spring Data Rest: on ajoute cette ligne pour afficher les ids lors de l'appel de http://localhost:8080/students
//		repositoryRestConfiguration.exposeIdsFor(Formation.class, Student.class);
//		
//		Formation f1 = new Formation("JAVA", 60);
//		Formation f2 = new Formation("DotNet", 60);
//		
//		f1 = formationRepository.saveAndFlush(f1);
//		f2 = formationRepository.saveAndFlush(f2);
//	
//		Student s1 = new Student("Elghoul", "Amal", null,f1);
//		Student s2 = new Student("Elghoul", "nabil", null,f1);
//		Student s3 = new Student("Elghoul", "jihed", null,f2);
//		Student s4 = new Student("Elghoul", "doua", null,f2);
//		
//		repository.saveAndFlush(s1);
//		repository.saveAndFlush(s2);
//		repository.saveAndFlush(s3);
//		repository.saveAndFlush(s4);
//		
//		List<Student> allStudents = repository.findAll();
//		System.out.println("these are all students list  :) :) :)");
//		for (Student student : allStudents) {
//			System.out.println(student.getPrenom());
//		}
//		
//		System.out.println("these are students list that comtains AM  !!!!!! ");
//		List<Student> studentsWithAmString = repository.findByNomContains("gh");
//		for (Student s : studentsWithAmString) {
//			System.out.println(s.getNom());
//		}
//	}
}
