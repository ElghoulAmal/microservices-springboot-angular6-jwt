package com.students.Repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.students.entities.Student;

/**
 * @author Utilisateur
 *
 */
@RepositoryRestResource
public interface StudentRepository extends JpaRepository<Student, Long>{
	public List<Student> findByNomStartsWith(String nom);
	public List<Student> findByNomContains(String nom);
	public List<Student> findByFormationId(Long id);
}
